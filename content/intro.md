# About this manual

This “How-To” Manual is meant as an explanation and working example of the possibilities of making an online open textbook with the Jupyter books software. To see the possibilities of using Jupyter books for your content, we refer to the [demo book](https://interactivetextbooks.tudelft.nl/open-textbooks-demonstration/) by Timon Idema.

The first part of the manual provides instructions to get you going to create your first book. The second part of the manual contains useful references and further information.

There are many ways of working with Jupyter Books. For example through different IDE's or making more use of the command line instead of an IDE, using different commands or packages that have similar functions, etc. If you feel confident that you can achieve the same result as part of the manual through your own method, feel free to do so. This manual just outlines one way of achieving the desired result.

This document is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

If you have any questions or feedback about the Jupyter Books manual, please don't hesitate to visit our [contact page](additional/contact.md). Thank you for using Jupyter Books to write your interactive book!
