# Useful references

This page includes useful links to the most important features that this implementation of Jupyter Books can showcase through its various plugins. However, it's recommended to also read the complete official documentation in order to gain a comprehensive understanding of the capabilities at your disposal.

For information on the configuration file settings used in the template, see [template configuration](template.md).

## Official documentation
- [Official Documentation](https://jupyterbook.org/) 
- [MyST syntax cheat sheet](https://jupyterbook.org/en/stable/reference/cheatsheet.html) - **Refer to this for quick reference**

## Installations
- [VSCode](https://code.visualstudio.com)
- [Anaconda](https://www.anaconda.com/download)
- [Git](https://git-scm.com/downloads)

## General structure
- [How headers and sections map onto to book structure](https://jupyterbook.org/en/stable/structure/sections-headers.html)
- [Special content blocks](https://jupyterbook.org/en/stable/content/content-blocks.html)
- [Components and UI elements](https://jupyterbook.org/en/stable/content/components.html)
- [Hide or remove content](https://jupyterbook.org/en/stable/interactive/hiding.html)
- [Control the page layout](https://jupyterbook.org/en/stable/content/layout.html)
- [Syntax Extensions](https://myst-parser.readthedocs.io/en/latest/syntax/optional.html)
- [Execute and cache your pages](https://jupyterbook.org/content/execute.html)

## References / Citations
- [Get started with references](https://jupyterbook.org/en/stable/tutorials/references.html)
- [References and cross-references](https://jupyterbook.org/en/stable/content/references.html)
- [Citations and bibliographies](https://jupyterbook.org/en/stable/content/citations.html)


## Math
- [Math and equations](https://jupyterbook.org/en/stable/content/math.html)
- [Math and equations/Latex-style math](https://jupyterbook.org/en/stable/content/math.html#latex-style-math)
- [Proofs, Theorems, and Algorithms](https://jupyterbook.org/en/stable/content/proof.html)
- [Sphinx-proof Documentation](https://sphinx-proof.readthedocs.io/en/latest/)
- [SciPy documentation](https://docs.scipy.org/doc/scipy/)
- [NumPy documentation](https://numpy.org/doc/1.24/)

## Figures
- [Images and figures](https://jupyterbook.org/en/stable/content/figures.html)
- [Interactive data visualizations](https://jupyterbook.org/en/stable/interactive/interactive.html)


## Plots / Graphs
- [Matplotlib: Visualization with Python](https://matplotlib.org/)
- [Plotly Open Source Graphing Library for Python](https://plotly.com/python/)


## Comments
- [Commenting and annotating](https://jupyterbook.org/en/stable/interactive/comments.html)
- [Hypothesis](https://jupyterbook.org/en/stable/interactive/comments/hypothesis.html)
- [Hypothesis (Official Website)](https://web.hypothes.is/)
- [Announcement banners](https://jupyterbook.org/en/stable/web/announcements.html)


## Exercises
- [Sphinx-exercise Documentation](https://ebp-sphinx-exercise.readthedocs.io/en/latest/)
- [JupyterQuiz GitHub](https://github.com/jmshea/jupyterquiz)
