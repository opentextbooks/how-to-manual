# Contact
Whether you have a question about a particular chapter or section of the manual, or you simply want to share your thoughts on your experience with Jupyter Books, we would love to hear from you.

So, please feel free to reach out to us by sending us an email at [interactive-textbooks@tudelft.nl](mailto:interactive-textbooks@tudelft.nl).