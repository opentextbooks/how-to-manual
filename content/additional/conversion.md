# Conversion from Word or LaTeX

If you have a book that is already written in LaTeX or Microsoft Word format, you can convert it into a Jupyter Book with our developed conversion scripts. The conversion process can be done in a few clicks using these scripts.


## LaTeX to Jupyter Book
A python script has been developed to convert your LaTeX files to MarkDown files for a Jupyter Book. It can be accessed [here](https://gitlab.tudelft.nl/opentextbooks/latex-to-markdown-conversion).

:::{admonition} LaTeX template
:class: warning, dropdown
For optimal conversion performance, your LaTeX content should be written following the [template LaTeX document](https://gitlab.tudelft.nl/opentextbooks/latex-template). The script should be able to convert all LaTeX files in this template (except the `main` file) to MarkDown. The template shows how to include image source files, problem source files and other commonly used features. Please note that the `main` file of the LaTeX template only includes other files and LaTeX settings; this file cannot (and need not) be converted to MarkDown (and will throw an error if you try).
:::

You can either download the `latextomarkdown.py` or clone [the repository](https://gitlab.tudelft.nl/opentextbooks/latex-to-markdown-conversion) onto your local system, where your LaTeX book exists. Each LaTeX file (chapter) must be individually converted using the script. 

:::{admonition} `cleanlatex.py`
:class: note
Before running the conversion script, you can clean up your LaTeX files using the `cleanlatex.py` script from [the same repository](https://gitlab.tudelft.nl/opentextbooks/latex-to-markdown-conversion). The script does not change the output of the LaTeX, but it removes comments and superfluous whitespace and changes the syntax of linebreaks for smoother conversion.
:::

In `latextomarkdown.py`, edit the basepath in the main function to point to the folder of the chapter to be converted. 

Then, run the script in the command line and provide the .tex filename as the argument.

    python3 latextomarkdown.py myfirstchapter
    
Note that you only need to provide the file name without the .tex extension as shown above.

The markdown file (.md) will be generated and stored in the same folder alongside the .tex file. 


```{admonition} PDF generation
:class: warning
Jupyter Book is able to generate a PDF. However, if you have a LaTeX file it is beter to construct your PDF from there.
```

## Word to Jupyter Book

A workflow together with a python script has been developed to convert your Microsoft Word files to MarkDown files for a Jupyter Book. The description of the steps of the workflow and the script can be found on [this repository](https://gitlab.tudelft.nl/opentextbooks/word-to-markdown-conversion). The output depends on the content in your Word documents, does it not guarantee a good conversion. In general, it helps to keep the Word documents simple and clean. The python script is still very basic and can be adjusted to perform better on specific content. 

For support with the conversion of Microsoft Word files to Jupyter Book, please [contact](contact.md) us.