# Adding a translation
If your book has a Dutch and an English version you can take the following steps to create a language switch in your Jupyter Book.

Organize your book with the following folders tree:

	├── book_EN
	|	├── _static
	|	|	├── img
	|	|	|	├── nl.png
	|	|	|	└── en.png
	|	|	|
	|	|	└── translation.js
	|	|
	|	├── _config.yml
	|	├── _toc.yml
	|	└── other files/folders ....		
	|	
	└──	book_NL
		├── _static
		|	├── img
		|	|	├── nl.png
		|	|	└── en.png
		|	|
		|	└── translation.js
		|
		├── _config.yml
		├── _toc.yml
		└── other files/folders ...	
	
and make sure that the `_static` folder is the same in the two book folders (`book_EN` and `book_NL`).

**You can find the content of the `_static` folder in this [repository](https://gitlab.tudelft.nl/opentextbooks/language-switch)**

Navigate to the `book_EN` folder and build the book.
If you don't have python installed make sure to activate the virtual environment.

```bash
cd <path>/book_EN
jupyter-book build .
```

Navigate to the `book_NL` folder and build that book too.

```bash
cd ../book_NL
jupyter-book build .
```

at this point the folder tree should look like this

	├── book_EN
	|	├── _build
	|	|	├── .doctrees
	|	|	└── html		
	|	|
	|	├── _static
	|	|	├── img
	|	|	|	├── nl.png
	|	|	|	└── en.png
	|	|	|
	|	|	└── translation.js
	|	|
	|	├── _config.yml
	|	├── _toc.yml
	|	└── other files/folders ....		
	|	
	└──	book_NL
		├── _build
		|	├── .doctrees
		|	└── html		
		|
		├── _static
		|	├── img
		|	|	├── nl.png
		|	|	└── en.png
		|	|
		|	└── translation.js
		|
		├── _config.yml
		├── _toc.yml
		└── other files/folders ...	
		
rename the `html` folder of the dutch book to `nl`.
Assuming you are still in the `book_NL` from the building command

```bash
cd _build
mv html nl
```

move the renamed folder inside the html folder of the english book

```bash
cp -r nl ../../book_EN/_build/html
```

browse the jupyter book with 2 languages at

	└── book_EN
		└── _build
			└── html
				└── index.html
