# Template Book

We created a [GitLab template repository](https://gitlab.tudelft.nl/opentextbooks/template-book) containing an appropriate folder structure and template versions of the files you will need for your JupyterBook. You can start with a copy of this repository, add your content files and adjust the configurations.

## Content
You can store your MarkDown files with content in the folder called `content`. In the template book, this folder contains two templates: `template-chapter.md` and `template-subchapter.md` for your content. Each MarkDown file will eventually be one webpage in your JupyterBook, take this into account when you divide your content over the MarkDown files. For more examples of what features you can include in your content, you can check out [our demobook](https://interactivetextbooks.tudelft.nl/open-textbooks-demonstration/).

## Requirements file
The `requirements.txt` file should contain a list of the libraries, modules, and packages that are required for your book. The packages `myst_nb` and `jupyterquiz` are required for building a jupyter book from MarkDown of Jupyter Notebook files. Additionally, other Sphinx modules that are used in your book should be included, such as `sphinx-exercise`, `sphinx-proof` and `sphinx-inline-tabs`. If you include Python code cells using libraries, such as `numpy`, `scipy`, `matplotlib` and `plotly`, they should be part of your `requirements.txt` file as well.


## Table of content file
You can structure your content for your Jupyter Book using the **YAML table of content file (`_toc.yml`)**. [Our template `_toc.yml` file](https://gitlab.tudelft.nl/opentextbooks/template-book/-/blob/main/_toc.yml) provides default settings for creating a new book, which you should modify to include your content. Detailed information on how to do this can be found [in a tutorial on the website of JupyterBook](https://jupyterbook.org/en/stable/structure/toc.html)



## Configuration file

You can configure your Jupyter Book using the **YAML configuration file (`_config.yml`)**.
This file controls a number of options and feature flags. Detailed information can be found [in a tutorial on the website of JupyterBook](https://jupyterbook.org/en/stable/customize/config.html)

This section is a reference for the `_config.yml` structure of our template implementation and how it should be changed when creating a new book.

```{caution}
YAML can be tricky when it comes to how it treats certain kinds of values. For example, when using strings in YAML, it is usually fine to omit quotation marks around them. However, there are certain values that will be *converted* to boolean values if they do not have strings around them, for example, `false`, `true`, `off`, etc. In addition, pure numbers will be converted to `float` or `int` unless you put strings around them.
```

[Our template `_config.yml` file](https://gitlab.tudelft.nl/opentextbooks/template-book/-/blob/main/_config.yml) provides default settings for creating a new book, which you can modify if necessary. These parameters are marked with an asterisk (`*`) in the following parameter lists and with the phrase `Change this parameter` in the `_config.yml` file for easy reference.

### Book settings

All the basic information regarding the book.

```yaml
# Book Settings
title: "Template Jupyter Book" 
author: Your Name 
description: This is the template book for everyone that wants to create a new Jupyter Book from scratch. 
copyright: Delft University of Technology, CC BY-SA 4.0
logo: content/images/tudelft.png
exclude_patterns: [ _build, Thumbs.db, .DS_Store, "**.ipynb_checkpoints" ]

# Only build files in the ToC to avoid building README, etc.
only_build_toc_files: true

```
```{admonition} Parameters
:class: seealso, dropdown

`title*`
: The title of the book. This will be placed in the left navigation bar.

`author*`
: The author of the book.

`description*`
: Text to indicate what the book is about.

`copyright`
: Copyright tag to be placed in the footer.

`logo`
: A path to the book logo.

`exclude_patterns`
: Patterns to skip when building the book. Any files that match the patterns described there will be excluded from the build.

`only_build_toc_files`
: Auto-exclude files not in the ToC and avoid building README, etc.
```

### Execution settings

**No need to change anything.** More information at: [Execute and cache your pages](https://jupyterbook.org/content/execute.html).


```yaml
# Execution Settings
execute:
  execute_notebooks: force
  cache: ""
  exclude_patterns: [ ]
  timeout: 30
  run_in_temp: false
  allow_errors: false
  stderr_output: show
```
```{admonition} Parameters
:class: seealso, dropdown
`execute_notebooks`
: This indicates whether to execute notebooks at build time. This must be one of "auto", "force", "cache" or "off".

`cache`
: A path to the Jupyter cache that will be used to store execution artifacts. Defaults to `_build/.jupyter_cache/`.

`exclude_patterns`
: A list of patterns to *skip* in execution (e.g. a notebook that takes a really long time).

`timeout`
: The maximum time (in seconds) that each notebook cell is allowed to run.

`run_in_temp`
: If `True`, then a temporary directory will be created and used as the command working directory (cwd), otherwise the notebook's parent directory will be the cwd.

`allow_errors`
: If `False`, when a code cell raises an error, the execution is stopped; otherwise all cells are always run.

`stderr_output`
: This must be one of 'show', 'remove', 'remove-warn', 'warn', 'error' or 'severe'.
```

### Parse and render settings

**No need to change anything.** More information at: [Syntax Extensions](https://myst-parser.readthedocs.io/en/latest/syntax/optional.html).

```yaml
# Parse and Render Settings
parse:
  myst_enable_extensions:
    - amsmath
    - colon_fence # To allow for ::: instead of ``` blocks.
    - deflist
    - dollarmath
    - html_admonition
    - html_image
    - linkify
    - replacements
    - smartquotes
    - substitution
    - tasklist
  myst_url_schemes: [ mailto, http, https ]
  myst_dmath_double_inline: true
```
```{admonition} Parameters
:class: seealso, dropdown
`myst_enable_extensions`
: Extra plugins of MyST for more capabilities.

`myst_url_schemes`
: URI schemes that will be recognised as external URLs in MarkDown links.

`myst_dmath_double_inline`
: Allow display math ($$) within an inline context.
```

### HTML-Specific Settings

```yaml
# HTML-specific settings
html:
  favicon: "content/images/favicon.ico"
  use_edit_page_button: true
  use_repository_button: true
  use_issues_button: false
  use_multitoc_numbering: true
  extra_navbar: ""
  extra_footer: ""
  google_analytics_id: ""
  home_page_in_navbar: true
  baseurl: ""
  comments:
    hypothesis: true
    utterances: false
  announcement: ""
```
```{admonition} Parameters
:class: seealso, dropdown
`favicon`
: A path to a favicon image.

`use_edit_page_button`
: This indicates whether to add an "edit this page" button to pages. If `true`, repository information in repository: must be filled in.

`use_repository_button`
: This indicates whether to add a link to your repository button.

`use_issues_button`
: This indicates whether to add an "open an issue" button.

`use_multitoc_numbering`
: This ensures continuous numbering across parts/chapters.

`extra_navbar*`
: An extra navigation bar will be displayed underneath the left navigation bar (**optional**).

`extra_footer*`
: An extra footer will be displayed underneath the footer (**optional**).

`google_analytics_id`
: A Google Analytics (GA) ID that can be used to track book views.

`home_page_in_navbar`
: This indicates whether to include your home page in the left navigation bar.

`baseurl`
: The base URL where your book will be hosted. This is used for creating image previews and social media.

`hypothesis`
: Hypothesis for comments.

`utterances`
: Utterances for comments.

`announcement*`
: This creates a banner announcement at the top of the site (**optional**).
```

### LaTeX-specific settings

```yaml
# LaTeX-Specific Settings
latex:
  latex_engine: pdflatex
  use_jupyterbook_latex: true
  latex_documents:
    targetname: book.tex
```

```{admonition} Parameters
:class: seealso, dropdown
Define the name of the LaTeX output file for PDF builds.

`latex_engine`
: Must be one of 'pdflatex' or 'xelatex' (recommended for unicode), 'luatex', 'platex', 'uplatex'.

`use_jupyterbook_latex`
: Use sphinx-jupyterbook-latex for pdf builds as default.

`targetname*`
: Name of the produced book from PDF build (**optional**).
```

### Launch button settings

**No need to change anything.**

```yaml
# Launch Button Settings
launch_buttons:
  notebook_interface : classic
```
```{admonition} Parameters
:class: seealso, dropdown
`notebook_interface`
: The interface interactive links will activate ["classic", "jupyterlab"].
```

### Information about where the book exists on the web

```yaml
# Information about where the book exists on the web
repository:
  url: https://gitlab.tudelft.nl/opentextbooks/how-to-manual/
  path_to_book: ""
  branch: master
```

```{admonition} Parameters
:class: seealso, dropdown
`url*`
: Online location of your book.

`path_to_book*`
: A path to your book's folder, relative to the repository root (**optional**).

`branch*`
: This indicates which branch of the repository should be used when creating links (**optional**).
```

### References

**No need to change anything.**

```yaml
# Add a bibtex file so that we can create citations
bibtex_bibfiles:
  - references.bib
```
```{admonition} Parameters
:class: seealso, dropdown
`bibtex_bibfiles`
: File with all the citations used in the book.
```

### Advanced and power-user settings

**No need to change anything.**

```yaml
# Advanced and power-user settings
sphinx:
  extra_extensions: [...]
  local_extensions: [...]
  recursive_update: false
  config: [...]
```

```{admonition} Parameters
:class: seealso, dropdown
`extra_extensions`
: A list of extra extensions to load by Sphinx (added to those already used by Jupyter Books).

`local_extensions`
: A list of local extensions to load by Sphinx, specified by "name: path" items.

`recursive_update`
: A boolean indicating whether to overwrite the Sphinx config (true) or recursively update (false).

`config`
: key-value pairs to directly over-ride the Sphinx configuration.
```



