# Git

## Version control

[Git](https://git-scm.com/) is a method for what software developers call version control, allowing you to efficiently store and share versions of your code (or whatever you're working on) and to collaborate on projects, with an easy way of integrating changes. Rather than making new copies of a document every time you add changes (a very common practice among scientists writing papers, alas), the git software keeps track only of the changes in your document, allowing you to step back however far you wish, and comparing versions - both with changes you made, and with changes made by collaborators. The 'undo' option in many other software packages does the same thing. Very likely you've unwittingly already worked with versions of git, for example when writing in Overleaf.

## GitLab

TU Delft hosts a local server with GitLab software, which is an implementation of git with a lot of useful extensions. Anyone with a TU Delft netid can get a GitLab account, simply by logging in on <https://gitlab.tudelft.nl>. You can then be added to projects or start your own.

The easiest way to work on a project on GitLab with version control is to use an integrated environment in a writing package, such as VSCode. The [website of VSCode](https://code.visualstudio.com/docs/sourcecontrol/intro-to-git) contains detailed instructions. The tutorial mentions GitHub, but you can clone your (TUDelft) GitLab projects in VSCode in the same way! You can start with an empty project on GitLab (<https://gitlab.tudelft.nl>), then clone it (with the provided key) in VSCode; you can then simply add files to the project folder, which VSCode will sync for you with your GitLab repository. If you have never used git, there are a few setup steps; plenty of online tutorials will guide you through them. 

Alternatively, you can upload a project from your local file system to GitLab using [this tutorial from Stackoverflow](https://stackoverflow.com/questions/70038069/how-to-upload-project-from-local-file-system-to-gitlab).

```{caution}
If you intend to publish your book through TUDelft, the repository with your (finalized) content needs to be part of the [Open Textbooks GitLab group](https://gitlab.tudelft.nl/opentextbooks). Please [contact us](../../additional/contact.md), so we can help you setting this up.
```

## Template

We created a [GitLab template repository](https://gitlab.tudelft.nl/opentextbooks/template-book) containing an appropriate folder structure and template versions of the files you will need for your JupyterBook. [This page](../../additional/template.md) contains more information on the files in the template repository. You can start with a copy of this repository if you are creating your book from scratch.

