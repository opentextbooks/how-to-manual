# Installations

Jupyter Books is a Python package so it is recommended to run this from an Integrated Development Environment (IDE). If you have your own preferred IDE feel free to use that. If you do not, we recommend VScode, which is opensource and free to use. This manual will detail the steps to take in VScode, which should be comparable to different IDE's.

To publish the book we will need to upload the files to the TU Delft gitlab server so we will also need to work with Git. Through Git is also how you will be able to make changes to your book while it is already online.
