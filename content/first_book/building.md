# Building your book
It is recommended that you first follow the steps outlined on the [Jupyter Book website](https://jupyterbook.org/en/stable/start/your-first-book.html) starting from 'overview' through 'create your own content file' to get a feel for how a Jupyter Book is structured. In general, the Jupyter Book website is a useful reference.

Having familiarised yourself with the various files used in a Jupyter Book, you can start creating your own book. We have prepared a template book with standard configurations in the `_config.yml` file. You can download it from [our template repository](https://gitlab.tudelft.nl/opentextbooks/template-book), add your files and change the `_toc.yml` accordingly. [This page](../additional/template.md) contains more information on the files in the template repository.

On top of the files outlined in the steps on the Jupyter Book website: content, config and toc, the template book also contains a `requirements.txt` file. This file contains all the packages required to run the Jupyter Book, including the Jupyter Books package itself. In order to build your book locally, you need to download these packages. It is recommended to download these packages into a closed environment as outlined below to 1) prevent installation of the packages directly into your computer, cluttering it up and 2) ensuring that you have a clear overview of the packages your Jupyter Books require.




(create-env)=
## Create an environment

In order to build your books, you have to create a closed environment for the necessary packages to be downloaded. There are several ways to achieve this. For the purpose of this tutorial, we will use a Conda environment.

First, navigate your command line to the root path of the book. Then, you can create and activate an environment by running the following commands (preferably one by one):

```bash
conda create --name=<env-name> 
conda activate <env-name>
conda install pip
pip install -r requirements.txt
```

In principle, the previous command will create and activate the environment. If something goes wrong, see if you can find a solution for the error messages online, or (if you cannot find a solution) [get in touch with us](../additional/contact.md).


:::{admonition} Delete an environment
:class: seealso, dropdown

If you want to remove an environment, run the following command:

```bash
conda remove --name <env-name> --all
```
:::

## Active environment

Every time you want to build your build again, make sure you have the right environment activated by running the following command:

```bash
conda activate <env-name>
```

:::{admonition} Deactivate an environment
:class: seealso, dropdown

If you want to deactivate an environment, run the following command:

```bash
conda deactivate <env-name>
```
:::

## Building your book locally

Having installed all the necessary packages, you can now build the book locally by simply running the command:

```bash
jupyter-book build <book-path>
```

Which will generate your book as a fully functioning HTML website on your local computer. You can access it by navigating to that folder and opening it with your web browser. In case you want to create a pdf version of your book, you can use the following command:

```bash
jupyter-book build <book-path> --builder pdfhtml
```
Please keep in mind that the layout of this pdf is not as good as the HTML website. However, this pdf is sufficient for copyright checks.
