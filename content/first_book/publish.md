# Publishing your book

To publish your book online, first locally build your book as explained in the previous chapter and review your content. There should be a collection of HTML files in your book's `_build/html` folder. To update your HTML files and make changes to your book's content, re-build your book with `jupyter-book build --all <book-path>`.


## Open resource
Your book will be published as an open resource by TU Delft. As an author of Open Interactive Textbooks, you need to declare that you have taken or intend to take the steps stated in the [quality checklist Open Interactive Textbooks](https://view.officeapps.live.com/op/view.aspx?src=https%3A%2F%2Ffilelist.tudelft.nl%2FLibrary%2FThemaportalen%2FOpen.tudelft.nl%2Fpublishing%2Ftextbooks%2FQuality-checklist_OIT.docx&wdOrigin=BROWSELINK) to ensure the quality of your Open Interactive Textbook.


## Putting your work on a repository

In order to get your book published as open interactive textbook by the TU Delft Library, your content needs to be in a GitLab repository of the [Open Textbooks GitLab group](https://gitlab.tudelft.nl/opentextbooks). Send us an email on interactive-textbooks@tudelft.nl, so we can make a repository for you to upload your files or copy your repository to the group. 

:::{note}
The files in your repository should be structured like the [GitLab template repository](https://gitlab.tudelft.nl/opentextbooks/template-book). This means that you should not upload your local `_build` folder to GitLab. Once your book is published, it will be built automatically by the server. [This page](../additional/template.md) contains more information on the files in the template repository. You can start with a copy of this repository if you are creating your book from scratch.
:::

```{admonition} Book versions
:class: tip, dropdown
Once your book is published, the version of your book on the main branch will automatically be hosted online. If you want to separate the book in versions every time you update your book, you can state the current version in the branch name (`<book-version-branch>`) and at the commit message (`<version>`)! Because the book should be checked for plagiarism and copyright, you need to get in touch with the library before making substantial changes once your book is online.  
```


## Next steps

There are a few more steps in order to get your book published: 
- Create a pdf of your book (see [this page](https://interactivetextbooks.tudelft.nl/how-to-manual/content/first_book/building.html#building-your-book-locally)) and send this to interactive-textbooks@tudelft.nl in order to get a copyright and plagiarism check. Also indicate if you want your text to be checked by an editor for spelling and grammar. 
- Update your book in accordance with the requested changes from the copyright and plagiarism check.
- Find an image for your book cover, fill in [the catalogue information form](https://filelist.tudelft.nl/Library/Themaportalen/Open.tudelft.nl/publishing/textbooks/Catalogue-information-form_OIT.docx) and send it to interactive-textbooks@tudelft.nl 



